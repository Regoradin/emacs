;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
(package-initialize)


(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

(server-start)

(menu-bar-mode -1)
(tool-bar-mode -1)
(setq inhibit-splash-screen t)

(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
(package-initialize)

(load-theme 'cobalt t t)
(enable-theme 'cobalt)

(setq ring-bell-function 'ignore)

(add-hook 'csharp-mode-hook 'omnisharp-mode)
;;(add-hook 'csharp-mode-hook 'omnisharp-start-omnisharp-server)


(defun my-csharp-mode-hook ()
  (setq c-basic-offset 4)
  (local-set-key "{" 'c-open-brace)
  (electric-pair-mode 1)
  
  (setq indent-tabs-mode nil)

  (omnisharp-mode)
  (company-mode)
  (flycheck-mode)
  )

(add-hook 'csharp-mode-hook 'my-csharp-mode-hook)
(eval-after-load
    'company
  '(add-to-list 'company-backends 'company-omnisharp))
(add-hook 'csharp-mode-hook #'company-mode)

(defun c-open-brace ()
  (interactive)
  (let ((electric-pair-mode nil))
      (insert "\n") 
      (insert "{")
      (indent-according-to-mode)
      (insert "\n")
      (insert "\n")
      (insert "}") 
      (indent-according-to-mode)
      (forward-line -1)
      (indent-according-to-mode)
      )
  )

;;Unity Specific Config
(add-to-list 'completion-ignored-extensions ".meta")

;;Magit Config
(global-set-key (kbd "C-x g") 'magit-status)
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(company-show-numbers ''t)
 '(company-transformers '(company-sort-by-occurrence))
 '(package-selected-packages
   '(gdscript-mode yaml-mode csv-mode magit company omnisharp csound-mode csharp-mode color-theme-modern)))

(require 'dired-x)
(setq-default dired-omit-files-p t) ; Buffer-local variable
(setq dired-omit-files (concat dired-omit-files "\\|^\\..+$"))

;;make company good
(setq company-idle-delay 0)
(setq company-minimum-prefix-length 3)

;;Default starter for new C# files (for Unity)
(setq auto-insert-query nil)
(add-hook 'find-file-hook 'auto-insert)
(eval-after-load 'autoinsert
  '(define-auto-insert '("\\.cs\\'" . "C# skeleton")
     '("This string gets ignored apparently"
       "using System.Collections;" \n
       "using System.Collections.Generic;" \n
       "using UnityEngine;" \n
       \n
       "public class " (file-name-nondirectory (file-name-sans-extension buffer-file-name)) " : MonoBehaviour" \n
       "{" > \n
       > _ \n
       "}" > \n
       )
     )
  )


;;somewhat hacky autocorrect-ish setup from https://emacs.stackexchange.com/questions/2793/is-it-possible-to-auto-correct-spelling-on-space
(define-key ctl-x-map "\C-i" 'endless/ispell-word-then-abbrev)
(defun endless/ispell-word-then-abbrev (p)
  "Call `ispell-word'. Then create an abbrev for the correction made.
With prefix P, create local abbrev. Otherwise it will be global."
  (interactive "P")
  (let ((bef (downcase (or (thing-at-point 'word) ""))) aft)
    (call-interactively 'ispell-word)
    (setq aft (downcase (or (thing-at-point 'word) "")))
    (unless (string= aft bef)
      (message "\"%s\" now expands to \"%s\" %sally"
               bef aft (if p "loc" "glob"))
      (define-abbrev
        (if p local-abbrev-table global-abbrev-table)
        bef aft))))

(setq save-abbrevs t)
(setq-default abbrev-mode t)

(require 'gdscript-mode)
